package game;


public class Mana {
	private int maxMana;
	private int mana;
	private int regenManaCooldown;
	private int regenManaPer1000;

	public int getMaxMana() {
		return maxMana;
	}

	public void setMaxMana(int maxMana) {
		this.maxMana = maxMana;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public int getRegenManaPer1000() {
		return regenManaPer1000;
	}

	public void setRegenManaPer1000(int regenManaPer1000) {
		this.regenManaPer1000 = regenManaPer1000;
	}

	public void modifyMana(int amount) {
		mana = Math.max(0, Math.min(mana + amount, maxMana));
	}

	public void regenerateMana(Creature creature) {
		regenManaCooldown -= regenManaPer1000;
		if (regenManaCooldown < 0) {
			if (mana < maxMana) {
				modifyMana(1);
				creature.modifyFood(-1);
			}
			regenManaCooldown += 1000;
		}
	}
}